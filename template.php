<?php

/**
 * Implements hook_ctools_plugin_api().
 */
function bartik_everywhere_ctools_plugin_api($module, $api) {
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function bartik_everywhere_ctools_plugin_directory($module, $type) {
  if (in_array($type, array('arguments', 'contexts', 'tasks'))) {
    return "plugins/$type";
  }
}

function bartik_everywhere_ctools_plugin_post_alter(&$plugin, &$info) {
  // Override a function defined by the plugin.
  if (isset($plugin['render callback'])) {
    switch ($plugin['render callback']) {
      case 'ctools_page_logo_content_type_render':
        $plugin['render callback'] = 'bartik_everywhere_page_logo_content_type_render';
        break;
    }
  }
}


/**
 *
 */
function bartik_everywhere_page_logo_content_type_render($subtype, $conf, $panel_args) {
  $logo = theme_get_setting('logo', 'bartik');
  $block = new stdClass();

  if (!empty($logo)) {
    $image = '<img src="' . $logo . '" alt="' . t('Home') . '" />';
    $block->content = l($image, '', array('html' => TRUE, 'attributes' => array('rel' => 'home', 'id' => 'logo', 'title' => t('Home'))));
  }
  return $block;
}


function bartik_everywhere_preprocess_pane_header(&$vars) {
  $vars['main_menu'] = menu_main_menu();
  $vars['secondary_menu'] = menu_secondary_menu();
  $vars['logo'] = theme_get_setting('logo', 'bartik');
}

function bartik_everywhere_process_pane_header(&$vars) {
  $vars['main_menu'] = theme('links__system_main_menu', array(
    'links' => $vars['main_menu'],
    'attributes' => array(
      'id' => 'main-menu-links',
      'class' => array('links', 'clearfix'),
    ),
    'heading' => array(
      'text' => t('Main menu'),
      'level' => 'h2',
      'class' => array('element-invisible'),
    ),
  ));
  $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
    'links' => $vars['secondary_menu'],
    'attributes' => array(
      'id' => 'secondary-menu-links',
      'class' => array('links', 'inline', 'clearfix'),
    ),
    'heading' => array(
      'text' => t('Secondary menu'),
      'level' => 'h2',
      'class' => array('element-invisible'),
    ),
  ));
}