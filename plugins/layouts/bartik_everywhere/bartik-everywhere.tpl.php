<div id="page-wrapper">
  <div id="page">
    <div id="header">
      <?php print $content['header']; ?>
    </div>
    <div id="main-wrapper" class="clearfix">
      <div id="main" class="clearfix">
        <div id="content" class="column">
          <div class="section">
            <?php print $content['content']; ?>
          </div>
        </div>
      </div>
    </div>
    <div id="footer-wrapper">
      <div class="section">
        <?php print $content['footer']; ?>
      </div>
    </div>
  </div>
</div>
