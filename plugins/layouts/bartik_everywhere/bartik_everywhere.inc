<?php

/**
 * Implementation of hook_panels_layouts().
 */
$plugin = array(
  'title' => t('Bartik Everywhere'),
  'category' => t('Theme: Bartik Everywhere'),
  //'icon' => 'tinsel.png',
  'theme' => 'bartik_everywhere',
  //'admin theme' => 'barmimik_admin',
  // 'css' => 'tinsel.css',
  //'admin css' => 'tinsel-admin.css',
  'regions' => array(
    'header' => t('Header'),
    'content' => t('Content'),
    'footer' => t('Footer'),
  ),
);

/**
 * Override or insert bartik_everywhere variables into the templates.
 */
function template_preprocess_bartik_everywhere(&$vars) {

  /*
  $vars['barmimik_classes'] = '';
  if ($vars['content']['left'] != '' && $vars['content']['right'] != '') {
    $vars['barmimik_classes'] = 'sidebars';
  }
  else {
    if ($vars['content']['left'] != '') {
      $vars['barmimik_classes'] .= ' sidebar-left';
    }
    if ($vars['content']['right'] != '') {
      $vars['barmimik_classes'] .= ' sidebar-right';
    }
  }
  */
}
